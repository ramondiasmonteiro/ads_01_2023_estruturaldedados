/*
Exercício EDUPT18 - calendario
Curso: IFTM - ADS - 2023/01

Aluno: Ramon Dias Monteiro Silva
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct infos
{
    int valor;
    char nome[30];
} Infos;

typedef struct no
{
    Infos dados;
    struct no *prox;
} No;

//======= FUNÇÕES DA LISTA CIRCULAR =======
No *criaNo(Infos dados);
No *insereFim(No *L, Infos dados);
//======= FUNÇÕES DA LISTA CIRCULAR =======

int anoBissexto(int ano);
int diaDaSemana(int dia, int mes, int ano);
void funcaoPrincipal(int ano);
No *criaMes(No *mes);
No *criaDiaSemana(No *diaSemana);
No *criaDia(No *dia, int ano);
void mostraCalendario(int ano, No *mes, No *diaSemana, No *dia);

int main()
{
    int ano;

    scanf("%d", &ano);
    funcaoPrincipal(ano);
    return 0;
}

//======= FUNÇÕES DA LISTA CIRCULAR =======
No *criaNo(Infos dados)
{
    No *novo = NULL;
    novo = (No *)malloc(sizeof(No));

    novo->dados.valor = dados.valor;
    strcpy(novo->dados.nome, dados.nome);
    novo->prox = NULL;

    return novo;
}

No *insereFim(No *L, Infos dados)
{
    No *novo = criaNo(dados);
    No *aux = L;
    if (L == NULL)
    {
        L = novo;
        novo->prox = novo;
    }
    else
    {
        while (aux->prox != L)
        {
            aux = aux->prox;
        }
        aux->prox = novo;
        novo->prox = L;
    }
    return L;
}

//======= FUNÇÕES DA LISTA CIRCULAR =======

int anoBissexto(int ano)
{
    int resposta = 0;

    if ((ano % 4 == 0 && ano % 100 != 0) || (ano % 400 == 0))
    {
        resposta = 1;
    }
    return resposta;
}

int diaDaSemana(int dia, int mes, int ano)
{
    int f = ano + dia + 3 * (mes - 1) - 1;

    if (mes < 3)
        ano--;

    else
        f -= (int)(0.4 * mes + 2.3);

    f += (int)(ano / 4) - (int)((ano / 100 + 1) * 0.75);

    f %= 7;

    return f;
}

void funcaoPrincipal(int ano)
{
    No *mes = NULL;
    No *diaSemana = NULL;
    No *dia = NULL;

    mes = criaMes(mes);
    diaSemana = criaDiaSemana(diaSemana);
    dia = criaDia(dia, ano);
    mostraCalendario(ano, mes, diaSemana, dia);
}

No *criaMes(No *mes)
{
    Infos dados;
    for (int i = 1; i <= 13; i++)
    {
        switch (i)
        {
        case 1:
            dados.valor = i;
            strcpy(dados.nome, " Janeiro");
            mes = insereFim(mes, dados);
            break;

        case 2:
            dados.valor = i;
            strcpy(dados.nome, " Fevereiro");
            mes = insereFim(mes, dados);
            break;

        case 3:
            dados.valor = i;
            strcpy(dados.nome, " Marco");
            mes = insereFim(mes, dados);
            break;

        case 4:
            dados.valor = i;
            strcpy(dados.nome, " Abril");
            mes = insereFim(mes, dados);
            break;

        case 5:
            dados.valor = i;
            strcpy(dados.nome, " Maio");
            mes = insereFim(mes, dados);
            break;

        case 6:
            dados.valor = i;
            strcpy(dados.nome, " Junho");
            mes = insereFim(mes, dados);
            break;

        case 7:
            dados.valor = i;
            strcpy(dados.nome, " Julho");
            mes = insereFim(mes, dados);
            break;

        case 8:
            dados.valor = i;
            strcpy(dados.nome, " Agosto");
            mes = insereFim(mes, dados);
            break;

        case 9:
            dados.valor = i;
            strcpy(dados.nome, " Setembro");
            mes = insereFim(mes, dados);
            break;

        case 10:
            dados.valor = i;
            strcpy(dados.nome, " Outubro");
            mes = insereFim(mes, dados);
            break;

        case 11:
            dados.valor = i;
            strcpy(dados.nome, " Novembro");
            mes = insereFim(mes, dados);
            break;

        case 12:
            dados.valor = i;
            strcpy(dados.nome, " Dezembro");
            mes = insereFim(mes, dados);
            break;

        default:
            break;
        }
    }

    return mes;
}

No *criaDiaSemana(No *diaSemana)
{
    Infos dados;
    for (int i = 0; i <= 7; i++)
    {
        switch (i)
        {
        case 0:
            dados.valor = i;
            strcpy(dados.nome, "D");
            diaSemana = insereFim(diaSemana, dados);
            break;

        case 1:
            dados.valor = i;
            strcpy(dados.nome, "S");
            diaSemana = insereFim(diaSemana, dados);
            break;

        case 2:
            dados.valor = i;
            strcpy(dados.nome, "T");
            diaSemana = insereFim(diaSemana, dados);
            break;

        case 3:
            dados.valor = i;
            strcpy(dados.nome, "Q");
            diaSemana = insereFim(diaSemana, dados);
            break;

        case 4:
            dados.valor = i;
            strcpy(dados.nome, "Q");
            diaSemana = insereFim(diaSemana, dados);
            break;

        case 5:
            dados.valor = i;
            strcpy(dados.nome, "S");
            diaSemana = insereFim(diaSemana, dados);
            break;

        case 6:
            dados.valor = i;
            strcpy(dados.nome, "S");
            diaSemana = insereFim(diaSemana, dados);
            break;
        default:
            break;
        }
    }
    return diaSemana;
}

No *criaDia(No *dia, int ano)
{
    Infos dados;
    for (int i = 1; i <= 13; i++)
    {
        switch (i)
        {
        case 1:
            dados.valor = 31;
            strcpy(dados.nome, "Janeiro");
            dia = insereFim(dia, dados);
            break;

        case 2:
            strcpy(dados.nome, "Fevereiro");
            dados.valor = 28;
            if (anoBissexto(ano) == 1)
            {
                dados.valor = 29;
            }
            dia = insereFim(dia, dados);
            break;

        case 3:
            dados.valor = 31;
            strcpy(dados.nome, "Marco");
            dia = insereFim(dia, dados);
            break;

        case 4:
            dados.valor = 30;
            strcpy(dados.nome, "Abril");
            dia = insereFim(dia, dados);
            break;

        case 5:
            dados.valor = 31;
            strcpy(dados.nome, "Maio");
            dia = insereFim(dia, dados);
            break;

        case 6:
            dados.valor = 30;
            strcpy(dados.nome, "Junho");
            dia = insereFim(dia, dados);
            break;

        case 7:
            dados.valor = 31;
            strcpy(dados.nome, "Julho");
            dia = insereFim(dia, dados);
            break;

        case 8:
            dados.valor = 31;
            strcpy(dados.nome, "Agosto");
            dia = insereFim(dia, dados);
            break;

        case 9:
            dados.valor = 30;
            strcpy(dados.nome, "Setembro");
            dia = insereFim(dia, dados);
            break;

        case 10:
            dados.valor = 31;
            strcpy(dados.nome, "Outubro");
            dia = insereFim(dia, dados);
            break;

        case 11:
            dados.valor = 30;
            strcpy(dados.nome, "Novembro");
            dia = insereFim(dia, dados);
            break;

        case 12:
            dados.valor = 31;
            strcpy(dados.nome, "Dezembro");
            dia = insereFim(dia, dados);
            break;

        default:
            break;
        }
    }
    return dia;
}

void mostraCalendario(int ano, No *mes, No *diaSemana, No *dia)
{
    No *auxMes = mes;
    No *aux1Mes = mes;

    No *auxDiaSemana = diaSemana;
    No *aux1DiaSemana = diaSemana;

    No *aux1Dia = dia;

    int contador = 0;

    do // Laço dos MESES e das qts de DIAS de cada mês
    {
        printf("\n%s", aux1Mes->dados.nome); // Mostrar os meses
        printf("\n");
        do // Laço dos DIAS DA SEMANA
        {
            printf(" %s ", aux1DiaSemana->dados.nome); // Mostrar os dias da semana
            aux1DiaSemana = aux1DiaSemana->prox;
        } while (aux1DiaSemana != auxDiaSemana);
        printf("\n");

        contador = 0; // Contador para o dia ir certo nos dias da semana

        for (int i = 0; i < aux1Dia->dados.valor; i++) // Mostra os dias
        {
            if (i == 0) // O primeiro dia do mês é verificado em qual dia da semana ele cai
            {
                contador = diaDaSemana(1, aux1Mes->dados.valor, ano);
                for (int j = 0; j < contador; j++) // Dependendo do dia ele pula os espaços necessários
                {
                    printf("   ");
                }
            }

            if (i < 9) // Dependendo da qtd de dia ele muda a forma de mostrar, para ficar identado
            {
                printf(" %d ", i + 1);
            }
            else
            {
                printf(" %d", i + 1);
            }

            contador++;
            if (contador >= 7) // Se o contador der um número maior ou igual os dias da semana, ele pula a linha
            {
                printf("\n");
                contador = 0;
            }
        }
        printf("\n");
        aux1Mes = aux1Mes->prox; // Pula o mês
        aux1Dia = aux1Dia->prox; // Pula a qtd de dias dos mês também
    } while (aux1Mes != auxMes);
}