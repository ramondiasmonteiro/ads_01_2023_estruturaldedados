/*
Exercício EDUPT19 - Fila do show
Curso: IFTM - ADS - 2023/01

Aluno: Ramon Dias Monteiro Silva
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//============ STRUCTS ============
typedef struct nod
{
    char cadeia[100000];
    struct nod *prox;
    struct nod *ant;
} Nod;

typedef struct pilha
{
    Nod *ini, *fim;
} Pilha;
//============ STRUCTS ============

//============ FUNÇÕES PILHA ============
Pilha *criaPilha();
Nod *criaNo(char cadeia[]);
int estaVazia(Pilha *p);
void push(Pilha *p, char cadeia[]); // Empilha
Nod *pop(Pilha *p);                 // Desempilha
//============ FUNÇÕES PILHA ============

//============ RESOLUÇÃO ============
void operacao(Pilha *p, int T);
char confereCadeia(char cadeiaAtual[]);
int confereFecha(char caractere);
char confereCaracteres(char cadeiaAtual[]);
int confereAbre(char caractere);
// void mostraPilha(Pilha *p);
//============ RESOLUÇÃO ============

//============ MAIN ============
int main()
{
    Pilha *p = criaPilha();

    int T;
    char valor[100000];

    scanf("%d", &T);
    for (int i = 0; i < T; i++)
    {
        fflush(stdin);
        fgets(valor, 100000, stdin);
        push(p, valor);
    }

    operacao(p, T);

    return 0;
}
//============ MAIN ============

//============ FUNÇÕES PILHA ============
Pilha *criaPilha()
{
    Pilha *p = (Pilha *)malloc(sizeof(Pilha));
    p->ini = p->fim = NULL;
    return p;
}

Nod *criaNo(char cadeia[])
{
    Nod *novo = (Nod *)malloc(sizeof(Nod));
    printf("%s", cadeia);
    strcpy(novo->cadeia, cadeia);
    novo->ant = novo->prox = NULL;
    return novo;
}

int estaVazia(Pilha *p)
{
    if (p->ini == NULL)
        return 1;
    else
        return 0;
}

void push(Pilha *p, char cadeia[])
{
    Nod *novo = criaNo(cadeia);
    if (p->ini == NULL)
    {
        p->ini = p->fim = novo;
    }
    else
    {
        novo->ant = p->fim;
        p->fim->prox = novo;
        p->fim = novo;
    }
}

Nod *pop(Pilha *p)
{
    Nod *aux = p->fim;

    if (aux->ant == NULL)
    {
        p->ini = p->fim = NULL;
    }

    if (aux != NULL)
    {
        p->fim = aux->ant;
        p->fim->prox = NULL;
        aux->ant = NULL;
    }

    return aux;
}
//============ FUNÇÕES PILHA ============

//============ RESOLUÇÃO ============
void operacao(Pilha *p, int T)
{
    char cadeiaAtual[100000];
    char resposta;

    Pilha *pResp = criaPilha();

    for (int i = 0; i < T; i++)
    {
        strcpy(cadeiaAtual, pop(p));
        resposta = confereCadeia(cadeiaAtual);

        push(pResp, &resposta);
    }

    // mostraPilha(pResp);
}

char confereCadeia(char cadeiaAtual[])
{
    char resposta = 'N';

    if ((strlen(cadeiaAtual) % 2) != 0) // Se a qtd de caracteres for impar, a resposta já é N
    {
        return resposta;
    }
    if (confereFecha(cadeiaAtual[0]) != 0) // Se a qtd for par, confere se o primeiro caractere é de fechamento
    {
        return resposta;
    }

    resposta = confereCaracteres(cadeiaAtual);

    return resposta;
}

int confereFecha(char caractere)
{
    int confere = 0;

    if (caractere == ')')
    {
        confere = 1;
    }
    else if (caractere == ']')
    {
        confere = 2;
    }
    else if (caractere == '}')
    {
        confere = 3;
    }
    return confere;
}

char confereCaracteres(char cadeiaAtual[])
{
    char resposta = 'N';

    Pilha *pAux = criaPilha(); // Pilha auxiliar

    char cProx = cadeiaAtual[1]; // Próximo caractere na string cadeiaAtual;

    int contador = 0, codAbre = 0, codFecha = 0;

    while (cProx != '\0')
    {
        codFecha = confereFecha(&cProx);
        push(pAux, cadeiaAtual[contador]);
        if (codFecha != 0)
        {
            codAbre = confereAbre(pop(pAux));
            if (codAbre != codFecha)
            {
                return resposta;
            }
        }
        contador++;
        cProx = cadeiaAtual[contador + 1];
        codAbre = codFecha = 0;
    }

    if (estaVazia(pAux) == 0)
    {
        return resposta;
    }

    resposta = 'S';
    return resposta;
}

int confereAbre(char caractere)
{
    int confere = 0;

    if (caractere == '(')
    {
        confere = 1;
    }
    else if (caractere == '[')
    {
        confere = 2;
    }
    else if (caractere == '{')
    {
        confere = 3;
    }
    return confere;
}

// void mostraPilha(Pilha *p)
// {
//     while (estaVazia(p) != 1)
//     {
//         printf("\n%s", pop(p));
//     }
// }
//============ RESOLUÇÃO ============