/*
Exercício EDUPT17 - Reis do Xadrez
Curso: IFTM - ADS - 2023/01
Data: 09/05/2023

Aluno: Ramon Dias Monteiro Silva.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//========= STRUCTS L1 PARTIDAS =========
typedef struct info
{
    char nome1[30];
    float p1;
    char nome2[30];
    float p2;
} Info;

typedef struct nod
{
    Info valores;
    struct nod *prox;
    struct nod *ant;
} Nod;

typedef struct listad
{
    Nod *ini, *fim;
} Listad;
//========= STRUCTS L1 PARTIDAS =========

//========= STRUCTS L2 RESULTADOS =========
typedef struct infoColocacao
{
    char nome[30];
    float p;
    int colocacao;
} InfoColocacao;

typedef struct pontuacao
{
    InfoColocacao resultado;
    struct pontuacao *prox;
    struct pontuacao *ant;
} Pontuacao;

typedef struct listad2
{
    Pontuacao *ini, *fim;
} Listad2;
//========= STRUCTS L2 RESULTADOS =========

//========== FUNÇÕES L1 (PARTIDAS) ==========
Listad *cria_listad_L1();
Nod *cria_nod_L1(Info infos);
void insere_fim_listad_L1(Info infos, Listad *L);
Listad *libera_listad_L1(Listad *L);
//========== FUNÇÕES L1 (PARTIDAS) ==========

int verificaIgual(Nod *aux, Nod *aux1);
void retiraDuplicado(Listad *L);
void removeElemento(Nod *elemento, Listad *L);
void insere_L1_L2(Listad *L);
int procuraAntes(Listad2 *L, char nome[]);
void addPontuacao(Listad2 *L, char nome[], float pont);
void ordenar(Listad2 *L);
void ordenar1(Listad2 *L);
void ordemAlfabetica(Pontuacao *a, Pontuacao *b);
void rankear(Listad2 *L);

//========== FUNÇÕES L2 (RESULTADOS) ==========
Listad2 *cria_listad_L2();
Pontuacao *cria_nod_L2(InfoColocacao infos);
void insere_fim_listad_L2(InfoColocacao infos, Listad2 *L);
Listad2 *libera_listad_L2(Listad2 *L);
//========== FUNÇÕES L2 (RESULTADOS) ==========

//============= MAIN =============
int main()
{
    Listad *L1 = cria_listad_L1();
    Info scan;
    int N;
    scanf("%d", &N);
    for (int i = 0; i < N; i++)
    {
        scanf("%s %f %s %f", scan.nome1, &scan.p1, scan.nome2, &scan.p2);
        insere_fim_listad_L1(scan, L1);
    }

    retiraDuplicado(L1);
    insere_L1_L2(L1);
    return 0;
}
//============= MAIN =============

//========== FUNÇÕES L1 (PARTIDAS) ==========
Listad *cria_listad_L1()
{
    Listad *L = (Listad *)malloc(sizeof(Listad));
    L->ini = L->fim = NULL;
    return L;
}

Nod *cria_nod_L1(Info infos)
{
    Nod *novo = (Nod *)malloc(sizeof(Nod));
    strcpy(novo->valores.nome1, infos.nome1);
    novo->valores.p1 = infos.p1;
    strcpy(novo->valores.nome2, infos.nome2);
    novo->valores.p2 = infos.p2;
    novo->ant = novo->prox = NULL;
    return novo;
}

void insere_fim_listad_L1(Info infos, Listad *L)
{
    Nod *novo = cria_nod_L1(infos);
    if (L->ini == NULL)
    {
        L->ini = L->fim = novo;
    }
    else
    {
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

Listad *libera_listad_L1(Listad *L)
{
    Nod *aux;

    while (L->ini != NULL)
    {
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }
    free(L);
    return NULL;
}
//========== FUNÇÕES L1 (PARTIDAS) ==========

int verificaIgual(Nod *aux, Nod *aux1)
{
    int resposta = 0;
    if ((aux->valores.p1 == aux1->valores.p1) && (aux->valores.p2 == aux1->valores.p2))
    {
        if ((strcmp(aux->valores.nome1, aux1->valores.nome1) == 0) && (strcmp(aux->valores.nome2, aux1->valores.nome2) == 0))
        {
            resposta = 1;
        }
    }

    if ((aux->valores.p1 == aux1->valores.p2) && (aux->valores.p2 == aux1->valores.p1))
    {
        if ((strcmp(aux->valores.nome1, aux1->valores.nome2) == 0) && (strcmp(aux->valores.nome2, aux1->valores.nome1) == 0))
        {
            resposta = 1;
        }
    }

    return resposta;
}

void retiraDuplicado(Listad *L)
{
    Nod *aux = L->ini;
    Nod *aux1;

    while (aux != NULL)
    {
        aux1 = L->ini;
        while (aux1 != aux)
        {
            if (verificaIgual(aux, aux1) == 1)
            {
                removeElemento(aux, L);
                break;
            }
            aux1 = aux1->prox;
        }
        aux = aux->prox;
    }
}

void removeElemento(Nod *elemento, Listad *L)
{
    Nod *aux = elemento;
    Nod *ant = elemento->ant;
    Nod *prox = elemento->prox;
    if (prox == NULL)
    {
        ant->prox = NULL;
        L->fim = ant;
        free(aux);
    }
    else
    {
        prox->ant = ant;
        ant->prox = prox;
        free(aux);
    }
}

void insere_L1_L2(Listad *L)
{
    Listad2 *L2 = cria_listad_L2();
    Nod *aux = L->ini;
    InfoColocacao atual;
    while (aux != NULL)
    {
        if (procuraAntes(L2, aux->valores.nome1) == 0) // 0 -> Não existe ainda
        {
            strcpy(atual.nome, aux->valores.nome1);
            atual.p = aux->valores.p1;
            atual.colocacao = 0;
            insere_fim_listad_L2(atual, L2);
        }
        else // 1 -> Existe na colocação
        {

            addPontuacao(L2, aux->valores.nome1, aux->valores.p1);
        }

        if (procuraAntes(L2, aux->valores.nome2) == 0)
        {
            strcpy(atual.nome, aux->valores.nome2);
            atual.p = aux->valores.p2;
            atual.colocacao = 0;
            insere_fim_listad_L2(atual, L2);
        }
        else
        {
            addPontuacao(L2, aux->valores.nome2, aux->valores.p2);
        }
        aux = aux->prox;
    }
    ordenar(L2);
    rankear(L2);
    ordenar1(L2);

    printf("\n\n");
    Pontuacao *aux3 = L2->ini;
    while (aux3 != NULL)
    {
        printf("\n%s %.1f %i", aux3->resultado.nome, aux3->resultado.p, aux3->resultado.colocacao);
        aux3 = aux3->prox;
    }
}

int procuraAntes(Listad2 *L, char nome[])
{
    int resposta = 0;
    Pontuacao *aux = L->ini;

    if (L->ini == NULL)
    {
        resposta = 0;
    }
    else
    {
        while (aux != NULL)
        {
            if (strcmp(aux->resultado.nome, nome) == 0)
            {
                resposta = 1;
            }
            aux = aux->prox;
        }
    }
    return resposta;
}

void addPontuacao(Listad2 *L, char nome[], float pont)
{
    Pontuacao *aux = L->ini;
    while (aux != NULL)
    {
        if (strcmp(aux->resultado.nome, nome) == 0)
        {
            aux->resultado.p += pont;
            break;
        }
        aux = aux->prox;
    }
}

void ordenar(Listad2 *L) // Lógica interessante de ordenação
{
    Pontuacao *aux1, *aux2;
    aux1 = aux2 = L->ini;
    Pontuacao *maior;
    Pontuacao aux3;

    while (aux1 != NULL)
    {
        aux2 = aux1;
        maior = aux1;
        while (aux2 != NULL)
        {
            if (maior->resultado.p < aux2->resultado.p)
            {
                maior = aux2;
            }
            aux2 = aux2->prox;
        }
        aux3.resultado.p = aux1->resultado.p;
        strcpy(aux3.resultado.nome, aux1->resultado.nome);

        aux1->resultado.p = maior->resultado.p;
        strcpy(aux1->resultado.nome, maior->resultado.nome);

        maior->resultado.p = aux3.resultado.p;
        strcpy(maior->resultado.nome, aux3.resultado.nome);
        aux1 = aux1->prox;
    }
}
void ordenar1(Listad2 *L)
{
    Pontuacao *aux1, *aux2;
    aux1 = aux2 = L->ini;
    int a = 1;
    int b = L->fim->resultado.colocacao;

    while (a <= b)
    {
        aux1 = aux2;
        while (aux2 != NULL && aux2->resultado.colocacao == a)
        {
            aux2 = aux2->prox;
        }

        ordemAlfabetica(aux1, aux2);

        a++;
    }
}

void ordemAlfabetica(Pontuacao *ini, Pontuacao *fim)
{
    Pontuacao *aux1, *aux2;
    aux1 = aux2 = ini;
    Pontuacao *maior;
    Pontuacao aux3;

    while (aux1 != fim)
    {
        aux2 = aux1;
        maior = aux1;
        while (aux2 != fim)
        {
            if (strcmp(maior->resultado.nome, aux2->resultado.nome) > 0)
            {
                maior = aux2;
            }
            aux2 = aux2->prox;
        }
        aux3.resultado.p = aux1->resultado.p;
        strcpy(aux3.resultado.nome, aux1->resultado.nome);

        aux1->resultado.p = maior->resultado.p;
        strcpy(aux1->resultado.nome, maior->resultado.nome);

        maior->resultado.p = aux3.resultado.p;
        strcpy(maior->resultado.nome, aux3.resultado.nome);
        aux1 = aux1->prox;
    }
}

void rankear(Listad2 *L)
{
    Pontuacao *aux = L->ini;
    float max = aux->resultado.p;
    int n = 1;
    while (aux != NULL)
    {
        if (aux->resultado.p == max)
        {
            aux->resultado.colocacao = n;
        }
        else
        {
            n++;
            max = aux->resultado.p;
            aux->resultado.colocacao = n;
        }

        aux = aux->prox;
    }
}

//========== FUNÇÕES L2 (RESULTADOS) ==========
Listad2 *cria_listad_L2()
{
    Listad2 *L = (Listad2 *)malloc(sizeof(Listad2));
    L->ini = L->fim = NULL;
    return L;
}

Pontuacao *cria_nod_L2(InfoColocacao infos)
{
    Pontuacao *novo = (Pontuacao *)malloc(sizeof(Pontuacao));
    strcpy(novo->resultado.nome, infos.nome);
    novo->resultado.p = infos.p;
    novo->resultado.colocacao = infos.colocacao;
    novo->ant = novo->prox = NULL;
    return novo;
}

void insere_fim_listad_L2(InfoColocacao infos, Listad2 *L)
{
    Pontuacao *novo = cria_nod_L2(infos);
    if (L->ini == NULL)
    {
        L->ini = L->fim = novo;
    }
    else
    {
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

Listad2 *libera_listad_L2(Listad2 *L)
{
    Pontuacao *aux;

    while (L->ini != NULL)
    {
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }
    free(L);
    return NULL;
}
//========== FUNÇÕES L2 (RESULTADOS) ==========