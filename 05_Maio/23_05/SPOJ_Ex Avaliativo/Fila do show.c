/*
Exercício EDUPT19 - Fila do show
Curso: IFTM - ADS - 2023/01

Aluno: Ramon Dias Monteiro Silva
*/

#include <stdlib.h>
#include <stdio.h>

typedef struct nod
{
    int identificador;
    struct nod *prox;
    struct nod *ant;
} Nod;

typedef struct fila
{
    Nod *ini, *fim;
} Fila;

Fila *criaFila();
Nod *criaNo(int info);
void enqueue(int identificador, Fila *F);
Nod *dequeue(Fila *F);
void imprime(Fila *F);
Fila *retiraSairam(Fila *F1, Fila *F2);

int main()
{
    int N, M, identificador;
    N = M = 0;

    Fila *F1 = criaFila();
    Fila *F2 = criaFila();

    scanf("%d", &N);
    for (int i = 0; i < N; i++)
    {
        scanf("%d", &identificador);
        enqueue(identificador, F1);
    }

    scanf("%d", &M);
    for (int i = 0; i < M; i++)
    {
        scanf("%d", &identificador);
        enqueue(identificador, F2);
    }

    Fila *F3 = retiraSairam(F1, F2);

    imprime(F3);

    return 0;
}

Fila *criaFila()
{
    Fila *F = (Fila *)malloc(sizeof(Fila));
    F->ini = F->fim = NULL;
    return F;
}

Nod *criaNo(int identificador)
{
    Nod *novo = (Nod *)malloc(sizeof(Nod));
    novo->identificador = identificador;
    novo->ant = novo->prox = NULL;
    return novo;
}

void enqueue(int identificador, Fila *F)
{
    Nod *novo = criaNo(identificador);
    if (F->ini == NULL)
    {
        F->ini = F->fim = novo;
    }
    else
    {
        novo->ant = F->fim;
        F->fim->prox = novo;
        F->fim = novo;
    }
}

Nod *dequeue(Fila *F)
{
    Nod *aux = F->ini;
    Nod *retirado = NULL;

    if (aux->prox == NULL)
    {
        retirado = aux;
        F->ini = F->fim = NULL;

        return retirado;
    }

    if (aux != NULL)
    {
        retirado = aux;
        aux->prox->ant = NULL;
        F->ini = aux->prox;
    }

    retirado->prox = NULL;
    return retirado;
}

Fila *retiraSairam(Fila *F1, Fila *F2)
{
    Fila *auxF1 = criaFila();

    Nod *aux = F2->ini;  // Fila dos que sairam
    Nod *aux1 = F1->ini; // Fila original
    Nod *valor = NULL;

    while (F2->ini != NULL)
    {
        while (aux1->identificador != aux->identificador && aux1 != NULL) // Sai do laço, caso o identificador seja igual
        {
            valor = dequeue(F1);                  // Retiro o primeiro elemento, pois ele não é igual ao que saiu da fila
            enqueue(valor->identificador, auxF1); // Adiciono esse elemento na fila auxiliar
            aux1 = F1->ini;                       // Aponto o auxiliar para o começo da fila novamente
        }
        dequeue(F1);                            // Retira o elemento cujo identificador é igual
        if (F1->ini == NULL && F1->fim == NULL) // Se a F1 for vazia, ou seja, o último elemento foi o retirado
        {
            // Faço a fila auxiliar virar a minha fila F1
            F1->ini = auxF1->ini;
            F1->fim = auxF1->fim;
            // Zero a fila auxiliar para poder usar ela novamente
            auxF1->ini = auxF1->fim = NULL;
        }

        if (auxF1->ini != NULL) // Vai linkar a lista auxiliar na lista original apenas se ela não for vazia
        {
            F1->ini->ant = auxF1->fim;  // Aponto o começo da F1 para o final da fila auxiliar do F1
            auxF1->fim->prox = F1->ini; // Aponto o fim da fila auxiliar para o inicio da F1

            F1->ini = auxF1->ini; // Aponto o o inicio da F1 para o início da fila auxiliar do F1

            auxF1->ini = auxF1->fim = NULL; // Aponto os ponteiros de ini e fim, da fila auxiliar, para NULL (dei reset na fila auxiliar)
        }

        dequeue(F2);   // Retira o identificador já conferido
        aux = F2->ini; // Aponto o auxiliar para o começo da fila novamente
        aux1 = F1->ini;
    }

    return F1;
}

void imprime(Fila *F)
{
    Nod *aux = F->ini;

    while (aux != NULL)
    {
        printf("%d ", (dequeue(F))->identificador);
        aux = F->ini;
    }
}