#include "ListaD.h"

Listad* cria_listad(){
    Listad *L = (Listad*)malloc(sizeof(Listad));
    L->ini = L->fim = NULL;
    return L;
}

Nod* cria_nod(int info){
    Nod *novo = (Nod*)malloc(sizeof(Nod));
    novo->info = info;
    novo->ant = novo->prox = NULL;
    return novo;
}

void insere_inicio_listad(int info, Listad *L){
    Nod *novo = cria_nod(info);
    if (L->ini = L->fim = novo){
        L->ini = L->fim = novo;
    }else{
        novo->prox = L->ini;
        L->ini->ant=novo;
    }
}

void insere_fim_listad(int info, Listad *L){
    Nod* novo = cria_nod(info);
    if (L->ini==NULL){
        L->ini = L->fim = novo;
    }else{
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

void mostra_listad(Listad *L){
    Nod* aux = L->ini;
    while (aux != NULL){
        printf("%d",aux->info);
        aux = aux->prox;
    }
}

Listad* libera_listad(Listad *L){
    Nod *aux;

    while (L->ini != NULL){
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }
    free(L);
    return NULL;
}