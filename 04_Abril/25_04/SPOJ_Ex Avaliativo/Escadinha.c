/*
Exercício EDUPT16 - Escadinha
Curso: IFTM - ADS - 2023/01 

Aluno: Ramon Dias Monteiro Silva.
*/
#include <stdlib.h>
#include <stdio.h>

typedef struct nod{
    int info;
    struct nod *prox;
    struct nod *ant;
}Nod;

typedef struct listad{
    Nod *ini,*fim;
}Listad;

Listad* cria_listad();
Nod* cria_nod(int info);
void insere_fim_listad(int info, Listad *L);
Listad* libera_listad(Listad *L);
int escadinhas(Listad *L, int N);

int main(){
    int N,scan;
    Listad *L = cria_listad();

    scanf("%d",&N);
    for (int i = 0; i < N; i++){
        scanf("%d",&scan);
        insere_fim_listad(scan,L);
    }
    scan = escadinhas(L, N);
    printf("\n%d",scan);
    L = libera_listad(L);
    return 0;
}

Listad* cria_listad(){
    Listad *L = (Listad*)malloc(sizeof(Listad));
    L->ini = L->fim = NULL;
    return L;
}

Nod* cria_nod(int info){
    Nod *novo = (Nod*)malloc(sizeof(Nod));
    novo->info = info;
    novo->ant = novo->prox = NULL;
    return novo;
}

void insere_fim_listad(int info, Listad *L){
    Nod* novo = cria_nod(info);
    if (L->ini==NULL){
        L->ini = L->fim = novo;
    }else{
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

Listad* libera_listad(Listad *L){
    Nod *aux;

    while (L->ini != NULL){
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }
    free(L);
    return NULL;
}

int escadinhas(Listad *L, int N){
    int contador = 0;
    Nod *aux = L->ini;
    Nod *aux1 = L->ini;
    Nod *aux2 = L->ini->prox;
    int dif,dif_escadinha;

    if (N == 1){
        contador++;
    }else{
        dif = aux1->info - aux2->info;
        while (aux != NULL){
            dif_escadinha = aux1->info - aux2->info;
            while (dif == dif_escadinha && aux2->prox != NULL){
                dif = aux1->info - aux2->info;
                if (dif == dif_escadinha && aux2->prox != NULL){
                    aux1 = aux1->prox;
                    aux2 = aux2->prox;
                }
            }
            aux = aux2->prox;
            contador++;
        }
    }
    return contador;
}