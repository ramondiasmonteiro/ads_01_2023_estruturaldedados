/*
Minimum()/Maximum()
Retorna ponteiro p que o elemento de menor (maior) valor x 
se encontra na lista; caso não encontre, retorna p igual a NULL;
*/
#include "ListaD.c"

int main(){
    Listad *L = cria_listad();
    insere_inicio_listad(10,L);
    insere_inicio_listad(12,L);
    insere_inicio_listad(14,L);
    insere_inicio_listad(16,L);
    insere_fim_listad(20,L);
    insere_fim_listad(22,L);
    
    mostra_listad(L);

    Nod *p;
    p=minimum(L);
    printf("\nValor min: %d",p->info);

    p=maximum(L);
    printf("\nValor max: %d",p->info);
    L = libera_listad(L);
    return 0;
}
