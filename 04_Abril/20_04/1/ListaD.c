#include "ListaD.h"

Listad* cria_listad(){
    Listad *L = (Listad*)malloc(sizeof(Listad));
    L->ini = L->fim = NULL;
    return L;
}

Nod* cria_nod(int info){
    Nod *novo = (Nod*)malloc(sizeof(Nod));
    novo->info = info;
    novo->ant = novo->prox = NULL;
    return novo;
}

void insere_inicio_listad(int info, Listad *L){
    Nod *novo = cria_nod(info);
    if (L->ini == NULL){
        L->ini = L->fim = novo;
    }else{
        novo->prox = L->ini;
        L->ini->ant=novo;
        L->ini = novo;
    }
}

void insere_fim_listad(int info, Listad *L){
    Nod* novo = cria_nod(info);
    if (L->ini==NULL){
        L->ini = L->fim = novo;
    }else{
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

void mostra_listad(Listad *L){
    Nod* aux = L->ini;
    while (aux != NULL){
        printf("| %d | ",aux->info);
        aux = aux->prox;
    }
    printf("\n");
}

Listad* libera_listad(Listad *L){
    Nod *aux;

    while (L->ini != NULL){
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }
    free(L);
    return NULL;
}

void ordena_listad(Listad *L){
    Nod *aux1,*aux2;
    aux1=aux2=L->ini;
    Nod *maior;
    int aux3;

    while (aux1!=NULL){
        aux2=aux1;
        maior=aux1;
        while (aux2!=NULL){
            if (maior->info < aux2->info){
                maior=aux2;
            }
            aux2 = aux2->prox;
        }
        aux3 = aux1->info;
        aux1->info = maior->info;
        maior->info = aux3; 
        aux1 = aux1->prox;
    }
}

void inverte_listad(Listad *L){
    Nod *aux1,*aux2,*aux3;
    aux1 = aux2 = L->ini;
    aux3=L->fim;
    int aux4;

    while (aux1!=aux3){
        while (aux2!=aux3){
            aux4 = aux2->info;
            aux2->info = aux2->prox->info;
            aux2->prox->info = aux4;
            aux2 = aux2->prox;
        }
        aux2=aux1;
        aux3 = aux3->ant;
    }
}

Nod* minimum(Listad *L){
    Nod *p = NULL;
    Nod *aux = L->ini;
    if(L!=NULL){
        p = aux;
        while (aux!=NULL){
            if (aux->info < p->info){
                p=aux;
            }
            aux = aux->prox;
        }
    }
    return p;
}

Nod* maximum(Listad *L){
    Nod *p;
    p = NULL;
    Nod *aux = L->ini;
    if((L->ini==NULL || L->fim==NULL)!=1){
        p = aux;
        while (aux!=NULL){
            if (aux->info > p->info){
                p=aux;
            }
            aux = aux->prox;
        }
    }
    return p;
}