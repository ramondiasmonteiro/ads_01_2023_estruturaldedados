/*
Reverse()
Versão que inverte a posição dos elementos da lista sem criar uma nova lista
*/

#include "ListaD.c"

int main(){
    Listad *L = cria_listad();
    insere_inicio_listad(10,L);
    insere_inicio_listad(12,L);
    insere_inicio_listad(14,L);
    insere_inicio_listad(16,L);
    insere_fim_listad(20,L);
    insere_fim_listad(22,L);
    
    mostra_listad(L);

    L = libera_listad(L);
    return 0;
}