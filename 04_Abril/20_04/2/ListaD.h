#ifndef LISTAD_H
#define LISTAD_H

#include <stdlib.h>
#include <stdio.h>

typedef struct nod{
    int info;
    struct nod *prox;
    struct nod *ant;
}Nod;

typedef struct listad{
    Nod *ini,*fim;
}Listad;

Listad* cria_listad();
Nod* cria_nod(int info);
void insere_inicio_listad(int info, Listad *L);
void insere_fim_listad(int info, Listad *L);
void mostra_listad(Listad *L);
Listad* libera_listad(Listad *L);
void ordena_listad(Listad *L);
void inverte_listad(Listad *L);
Nod* minimum(Listad *L);
Nod* maximum(Listad *L);
void reverse_1(Listad *L);
#endif