/*
Aluno: Ramon Dias Monteiro Silva.
Turma: ADS IFTM 01/2023

1) Crie um programa que defina e utilize a estrutura conta mostrada no slide 13
Esse programa deve:
    - Ler os dados de duas contas;
    - Mostrar os dados de cada conta;
    - Permitir que se realize uma transferência de valores de uma conta para a outra;
    - Mostrar os dados de cada conta novamente.

Sugestões para as funções do programa:
    • 1 função para ler os dados de uma conta;
    • 1 função para mostrar os dados de uma conta;
    • 1 função para transferir valor de uma conta para outra.
*/
#include <stdio.h>
#include <stdlib.h>
#define max 2

typedef struct conta{
int num;
char nome[30];
float saldo;
}conta;

void menu (){
    int *opc=(int*)malloc(sizeof(int));
    conta *user=(conta*)malloc(sizeof(conta));
    do{
        system("cls");
        printf("==========MENU==========");
        printf("\n\n0 - Sair;");
        printf("\n1 - Abrir conta;");
        printf("\n\nSelecione a opcao: ");
        scanf("%i",opc);

        switch (*opc){
        case 0:
            break;
        case 1:
            break;
        
        default:
            printf("\nOpcao invalida... ");
            system("pause");
            break;
        }

        system("cls");
    } while (*opc!=0);


    //_______________Área de free's_______________
    free(opc);
}

int main(){
    menu();
        printf("Encerrando... \n\n");
        // system("pause");
    return 0;
}
