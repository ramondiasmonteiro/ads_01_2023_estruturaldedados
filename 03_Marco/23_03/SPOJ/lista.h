#ifndef LISTA_H
#define LISTA_H

#include <stdio.h>
#include <stdlib.h>

typedef struct no{
    int valor;
    struct no *prox;
}No;

No* cria_no(int valor);
No* insere_comeco (No* L, int valor);
No* insere_fim(No* L, int valor);
No* remover(No** L, int valor);
No* remove_inicio(No** L);
No* remove_fim(No** L);
No* insere_na_seg_pos(No* L, No* novo);
No* reverse(No* L);
void mostra_L (No* L);

#endif