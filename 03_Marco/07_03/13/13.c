/*
Aluno: Ramon Dias Monteiro Silva.
Turma: ADS - IFTM - 01/2023


13) Escreva uma estrutura para armazenar dados de um estacionamento. Ela deve ser capaz de
armazenar o número da chapa do carro, a marca, a hora de entrada e a hora de saida do
estacionamento. Utilize dois membros do tipo tempo, definido no exercício anterior, para as
horas de entrada e saída.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 3

//=========== STRUCT'S ===========
typedef struct tempo{
    int min;
    int hr;
}tempo;

typedef struct carro{
    char placa[8];
    char marca[15];
    tempo hr_chegada;
    tempo hr_saida;
    int indice; // 1 ou 0 (1 ocupado / 0 não ocupado)
    int  nmr_vaga;
}carro;

//=========== FUNÇÕES ===========
void relatorio(int *nmr_carros, carro car[MAX], carro car_saida[MAX*2], int *nmr_saida){
    system("cls");
    printf("============ RELATORIO ============");
    printf("\nCarros no estacionamento: %i",*nmr_carros);
    printf("\n\n========= VAGAS =========");
    for (int i=0; i<MAX; i++){
        if (car[i].indice==0){
            printf("\n\n______ NUMERO %i ______",car[i].nmr_vaga);
            printf("\n\nVAGA VAZIA. . . ");
            printf("\n_________________________");
        }else{
            printf("\n\n______ NUMERO %i ______",car[i].nmr_vaga);
            printf("\n\nPlaca: %s",car[i].placa);
            printf("\nMarca: %s",car[i].marca);
            printf("\nHora de chegada: %i:%i",car[i].hr_chegada.hr, car[i].hr_chegada.min);
            printf("\n_________________________");
        }
    }
    if (*nmr_saida!=0){
        printf("\n-------------------------");
        printf("\n\n========= SAIDA DE VEICULOS =========");
        printf("\nCarros que sairam do estacionamento: %i",*nmr_saida);
        for (int i=0; i!=(*nmr_saida); i++){
            printf("\n\n______ VAGA QUE OCUPOU: %i ______",car_saida[i].nmr_vaga);
            printf("\n\nPlaca: %s",car_saida[i].placa);
            printf("\nMarca: %s",car_saida[i].marca);
            printf("\nHora de chegada: %i:%i",car_saida[i].hr_chegada.hr, car_saida[i].hr_chegada.min);
            printf("\nHora de saida: %i:%i",car_saida[i].hr_saida.hr, car_saida[i].hr_saida.min);
            printf("\n_________________________");
        }
    }
    printf("\n\n");
    system("pause");
}

void saida_veiculo(int *nmr_carros, carro car[MAX], carro car_saida[MAX*2], int *nmr_saida){
    int *vaga_saida=(int*)malloc(sizeof(int));
    int *aux=(int*)malloc(sizeof(int));
    *aux=0;
    if (*nmr_carros==0){
        printf("\nNenhum carro estacionado. . . ");
        system("pause");
    }else{
        system("cls");
        printf("============ SAIDA DE VEICULO ============");
        printf("\n\nNumero da vaga liberada: ");
        scanf("%i",vaga_saida);
        for (int i=0; i<MAX; i++){
            if (*vaga_saida==car[i].nmr_vaga && car[i].indice==1){
                printf("\nHora de saida: ");
                printf("\nHora: ");
                scanf("%i",&car[i].hr_saida.hr);
                printf("Minuto: ");
                scanf("%i",&car[i].hr_saida.min);
                (*nmr_carros)--;

                car_saida[*nmr_saida].nmr_vaga=car[i].nmr_vaga;
                strcpy(car_saida[*nmr_saida].marca,car[i].marca);
                strcpy(car_saida[*nmr_saida].placa,car[i].placa);
                car_saida[*nmr_saida].hr_chegada.hr=car[i].hr_chegada.hr;
                car_saida[*nmr_saida].hr_chegada.min=car[i].hr_chegada.min;
                car_saida[*nmr_saida].hr_saida.hr=car[i].hr_saida.hr;
                car_saida[*nmr_saida].hr_saida.min=car[i].hr_saida.min;
                (*nmr_saida)++;

                car[i].indice=0;
                strcpy(car[i].marca," ");
                strcpy(car[i].placa," ");
                car[i].hr_chegada.hr=0;
                car[i].hr_chegada.min=0;
                car[i].hr_saida.hr=0;
                car[i].hr_saida.min=0;
                
                *aux=1;

                break;
            }
        }
    }
    if (*aux==0 && *nmr_carros!=0){
        printf("\n\nVaga vazia ou inexistente. . . ");
        system("pause");
    }

    free(vaga_saida);
    free(aux);
}

void entrada_veiculo(int *nmr_carros, carro car[MAX]){
    if (*nmr_carros==MAX){
        printf("\nNumero maximo de carros atingido. . . ");
        system("pause");
    }else{
        for (int i=0; i<MAX; i++){
            if (car[i].indice==0){
                system("cls");
                printf("============ ENTRADA DE VEICULO ============");
                printf("\n\nPlaca do veiculo: ");
                fflush(stdin);
                gets(car[i].placa);
                printf("\nMarca do veiculo: ");
                fflush(stdin);
                gets(car[i].marca);
                printf("\nHora de chegada: ");
                printf("\nHora: ");
                scanf("%i",&car[i].hr_chegada.hr);
                printf("Minuto: ");
                scanf("%i",&car[i].hr_chegada.min);
                (*nmr_carros)++; //Contador de veículos no estacionamento
                car[i].indice=1;
                break; //Saída do laço
            }
        }
    }
}

void menu(){
    int *opc=(int*)malloc(sizeof(int));
    int *nmr_carros=(int*)malloc(sizeof(int));
    carro *car=(carro*)malloc(MAX*sizeof(carro));
    carro *car_saida=(carro*)malloc((MAX*2)*sizeof(carro));
    int *nmr_saida=(int*)malloc(sizeof(int));

    *nmr_carros=0;
    *nmr_saida=0;
    for (int i=0; i<MAX; i++){ //Inicia o vetor das vagas
        car[i].indice=0;
        car[i].nmr_vaga=i;
        strcpy(car[i].marca," ");
        strcpy(car[i].placa," ");
        car[i].hr_chegada.hr=0;
        car[i].hr_chegada.min=0;
        car[i].hr_saida.hr=0;
        car[i].hr_saida.min=0;
    }
    for (int i=0; i<(MAX*2); i++){ //Inicia o vetor dos veiculos que sairam
        car_saida[i].indice=0;
        car_saida[i].nmr_vaga=i;
        strcpy(car_saida[i].marca," ");
        strcpy(car_saida[i].placa," ");
        car_saida[i].hr_chegada.hr=0;
        car_saida[i].hr_chegada.min=0;
        car_saida[i].hr_saida.hr=0;
        car_saida[i].hr_saida.min=0;
    }
    do{
        system("cls");
        printf("============ ESTACIONAMENTO ============");
        printf("\n\n0 - Sair;");
        printf("\n1 - Entrada de veiculo;");
        printf("\n2 - Saida de veiculo;");
        printf("\n3 - Relatorio.");
        printf("\n\nOpcao selecionada: ");
        scanf("%i",opc);
        
        switch (*opc){
        case 0:
            printf("\nFinalizando programa. . . ");
            break;
        
        case 1:
            entrada_veiculo(nmr_carros,car);
            break;
        
        case 2:
        saida_veiculo(nmr_carros,car,car_saida,nmr_saida);
            break;
        
        case 3:
            relatorio(nmr_carros,car,car_saida,nmr_saida);
            break;

        default:
            printf("\n\nOpcao invalida!!! ");
            system("pause");
            break;
        }
    } while (*opc!=0);
    //=========== FREE'S ===========
    free(opc);
    free(nmr_carros);
    free(car);
    free(car_saida);
    free(nmr_saida);
}

//=========== MAIN ===========
int main(){
    menu();
    return 0;
}
