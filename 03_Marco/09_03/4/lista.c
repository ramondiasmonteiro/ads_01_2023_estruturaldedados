#include "lista.h"

No* cria_no(int valor){
    No* novo=NULL;
    novo=(No*)malloc(sizeof(No));

    novo->prox=NULL;
    novo->valor=valor;
    return novo;
}

No* insere_comeco (No* L, int valor){
    No* novo=cria_no(valor);
    novo->prox=L;
    L=novo;

    return L;
}

No* insere_fim(No* L, int valor){
    No* novo=cria_no(valor);
    No* aux=L;

    if (L == NULL){
        L=novo;
    }else{
        while (aux->prox != NULL){
            aux = aux->prox;
        }
        aux->prox = novo;
    }
    return L;
}

No* remover(No** L, int valor){
    No *aux=*L, *anterior=*L;

    while (aux->valor != valor){
        anterior = aux; 
        aux = aux->prox;
    }
    if(*L == aux){
        *L = (*L)->prox;
    } else{
        anterior->prox = aux->prox;
    }
    return aux;
}

int remove_inicio(No** L){
    No* aux=NULL;

    if(*L!=NULL){
        aux=*L;
        *L=(*L)->prox;
        aux->prox=NULL;
    }
    
    return aux->valor; //retorna o elemento removido
}

int remove_fim(No** L){
    No *aux=*L, *anterior=*L;

    while (aux->prox != NULL){
        anterior=aux;
        aux=aux->prox;
    }
    if(*L == aux){
        *L = (*L)->prox;
    } else{
        anterior->prox = NULL;
    }
    return aux->valor;
}

No* insere_na_seg_pos(No* L, No* novo){
    novo->prox = NULL;

    if (L == NULL)
       L = novo;
    else{
        if (L->prox == NULL)
            L->prox = novo;
        else
        {
            novo->prox = L->prox;
            L->prox = novo;
        }    
    }
    return L;
}

No* reverse(No* L){
    No *fim = L;
    No *elem;
    
    while (fim->prox != NULL){
        fim = fim->prox;
    }
    while (L != fim){
        elem = remove_inicio(&L);
        fim = insere_na_seg_pos(fim,elem);
    }
    return fim;
}

void mostra_L (No* L){
    No* aux;
    aux=L;

    printf("\n\n---- LISTA ----");
        if (L==NULL){
            printf("\n\nLISTA VAZIA. . . ");
        }else{
            printf("\n\n");
            while (aux != NULL){
                printf(" | %i | ",aux->valor);
                aux=aux->prox;
            }
        }
    printf("\n\n---------------");
}