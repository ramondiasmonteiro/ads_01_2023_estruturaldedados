/*
4:Altere as funções dos exercícios 2 e 3 para retornar o valor 
inteiro removido e não o ponteiro (nesse casso o espaço do nó 
deve ser liberado dentro das funções) 
*/

#include "lista.c"

int main(){
    No* L;
    int aux[2];
    L=NULL;

    L=insere_fim(L,10);
    L=insere_fim(L,20);
    L=insere_fim(L,30);
    L=insere_fim(L,40);
    mostra_L(L);

    aux[0] = remove_inicio(&L);

    aux[1] = remove_fim(&L);

    mostra_L(L);
    printf("\n\nValor retirado no inicio: %i",aux[0]);
    printf("\nValor retirado no fim:    %i",aux[1]);
    return 0;
}
