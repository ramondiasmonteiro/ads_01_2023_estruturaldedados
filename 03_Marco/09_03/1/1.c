/*
Aluno: Ramon Dias Monteiro Silva.
Turma: ADS - IFTM - 01/2023

1: Crie e teste uma função que insere um valor após um determinado elemento da lista. Os 
parâmetros devem ser a lista, o valor a inserir e o valor após o qual se deseja inserir.
Você deve tratar as seguintes situações: 
• Se a lista estiver vazia;
• Se o valor após o qual se deve inserir não estiver na lista;
• Você pode criar uma função só para localizar o elemento após o qual se quer inserir 
(busca_valor) que retorna um ponteiro para o nó que contém um valor determinado.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct no{
    int info;
    struct no *prox;
}No;

No* busca_valor(No* L, int old_valor, int new_valor){
    No* aux=L;
    No* r=NULL;
    while (aux != NULL){
        if (old_valor == aux->info){
            r=aux;
        }
        aux=aux->prox;
    }
    return r;
}

int verifica_valor(No* L, int old_valor){
    No* aux=L;
    int r=0;

    while (aux != NULL){
        if (old_valor == aux->info){
            r=1;
        }
        aux=aux->prox;
    }

    return r;
}

void mostra_L (No* L){
    No* aux;
    aux=L;

    printf("\n\n---- LISTA ----");
        if (L==NULL){
            printf("\n\nLISTA VAZIA. . . ");
        }else{
            printf("\n\n");
            while (aux != NULL){
                printf(" | %i | ",aux->info);
                aux=aux->prox;
            }
        }
    printf("\n\n---------------");
}

No* cria_no(int valor){
    No* novo=NULL;
    novo=(No*)malloc(sizeof(No));

    novo->prox=NULL;
    novo->info=valor;
    return novo;
}

No* insere_comeco (No* L, int valor){
    No* novo=cria_no(valor);
    novo->prox=L;
    L=novo;

    return L;
}

void insere_depois (No* aux, int valor){
    No* novo=cria_no(valor);
    if (aux->prox==NULL){
        aux->prox=novo;
    }else{
        novo->prox=aux->prox;
        aux->prox=novo;
    }
}

No* insere_fim(No* L, int valor){
    No* novo=cria_no(valor);
    No* aux=L;

    if (L == NULL){
        L=novo;
    }else{
        while (aux->prox != NULL){
            aux = aux->prox;
        }
        aux->prox = novo;
    }
    return L;
}

No* opc_3(No* L){
    int *new_valor=(int*)malloc(sizeof(int));

    system("cls");
    printf("\n========= ADICIONAR NO INICIO DA LISTA =========");
    mostra_L(L);
    printf("\n\nInforme o valor que voce quer adicionar: ");
    scanf("%i",new_valor);
    if (L==NULL){
        No* novo=cria_no(*new_valor);
        L=novo;
    }else{
        L=insere_comeco(L,*new_valor);
    }

    free(new_valor);

    return L;
}

No* opc_2(No* L){
    int *new_valor=(int*)malloc(sizeof(int));
    int *old_valor=(int*)malloc(sizeof(int));
    No* aux=NULL;

    system("cls");
    printf("\n========= ADICIONAR ELEMENTO APOS UM VALOR =========");
    mostra_L(L);
    printf("\n\nInforme o valor que voce quer adicionar: ");
    scanf("%i",new_valor);
    printf("\nInforme apos qual elemento voce quer adicionar o valor: ");
    scanf("%i",old_valor);
    if ((verifica_valor(L,*old_valor)==0)){
        printf("\n\nELEMENTO INEXISTENTE. . . ");
        system("pause");
    }else{
        aux=busca_valor(L,*old_valor,*new_valor);
        insere_depois(aux,*new_valor);
    }

    free(new_valor);
    free(old_valor);

    return L;
}

No* opc_1(No* L){
    int *valor=(int*)malloc(sizeof(int));

    system("cls");
    printf("\n========= ADICIONAR NO FIM DA LISTA =========");
    printf("\n\nValor a ser adicionado: ");
    scanf("%i",valor);
    
    L=insere_fim(L,*valor);
    
    free(valor);

    return L;
}

void menu(){
    int *opc=(int*)malloc(sizeof(int));
    No *L; //Criação da lista
    L=NULL;

    do{
        system("cls");
        printf("\n========= MENU =========");
        mostra_L(L);
        printf("\n\n0 - Sair;");
        printf("\n1 - Adicionar no fim da lista;");
        printf("\n2 - Adicionar elemento apos um valor;");
        printf("\n3 - Adicionar no inicio da lista.");
        printf("\n\nSelecione uma opcao: ");
        scanf("%i",opc);

        switch (*opc){
        case 0:
            printf("\n\nFechando programa. . . ");
            break;

        case 1:
            L=opc_1(L);
            break;
        
        case 2:
            if (L==NULL){
                printf("\n\nLista vazia!!! ");
                system("pause");
            }else{
                L=opc_2(L);
            }
            break;

        case 3:
            L=opc_3(L);
            break;
        
        default:
            break;
        }
    } while (*opc!=0);
    
    free(opc);
}

int main(){
    menu();
    return 0;
}
