/*
3: Crie e teste uma função que remove e retorna o nó do último  elemento da lista. 
*/

#include "lista.c"

int main(){
    No* L;
    No *aux;
    L=NULL;

    L=insere_fim(L,10);
    L=insere_fim(L,20);
    L=insere_fim(L,30);
    L=insere_fim(L,40);
    mostra_L(L);

    aux=remove_fim(&L);
    mostra_L(L);

    printf("\n\nValor retirado = %i",aux->valor);
    return 0;
}
