/*
Exercício EDUPT09 - DNA
Curso: IFTM - ADS - 2023/01 

Aluno 01: Ramon Dias Monteiro Silva.
Aluno 02: Diego Cardoso do Nascimento.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct no_lado{
    char dna[10];
    struct no_lado *prox_cima;
    struct no_lado *prox_lista;
}No_lado;

typedef struct contagem{
    char gene[10];
    int n;
    struct contagem *prox;
    struct contagem *lado;
}Contagem;


No_lado* cria_no(char dna[]);
No_lado* insere_fim(No_lado* L, char dna[]);
No_lado* cria_no_lado(No_lado* L);
No_lado* insere_fim_lado(No_lado* Lista_m, No_lado* L);
Contagem* contagem_de_genes(No_lado* L);
Contagem* cria_no_gene(int valor, char gene[]);
Contagem* insere_comeco_gene(Contagem* L_gene, int valor, char gene[]);
Contagem* cria_no_gene_lado(Contagem* count);
Contagem* insere_fim_gene(Contagem* count_m, Contagem* count);
void mostra_genes(Contagem* count_m);

int main(){
    int N=1,i;
    char scan[10];
    No_lado *L=NULL;
    No_lado *Lista_m=NULL;
    Contagem *count=NULL;
    Contagem *count_m=NULL;

    while (N!=0){
        scanf("%d",&N);
        getchar();//para funcionar com o input igual do SPOJ
        for (i=0; i<N; i++){
            //getchar();
            gets(scan);
            L=insere_fim(L,scan);
        }
        
        count=contagem_de_genes(L);
        count_m=insere_fim_gene(count_m,count);
        Lista_m=insere_fim_lado(Lista_m, L);
        count=NULL;
        L=NULL;
    }
    mostra_genes(count_m);
    return 0;
}

No_lado* cria_no(char dna[]){
    No_lado* novo=NULL;
    novo=(No_lado*)malloc(sizeof(No_lado));

    novo->prox_cima=NULL;
    novo->prox_lista=NULL;
    strcpy(novo->dna,dna);
    return novo;
}

No_lado* insere_fim(No_lado* L, char dna[]){
    No_lado* novo=cria_no(dna);
    No_lado* aux=L;

    if (L == NULL){
        L=novo;
    }else{
        while (aux->prox_cima != NULL){
            aux = aux->prox_cima;
        }
        aux->prox_cima = novo;
    }
    return L;
}

No_lado* cria_no_lado(No_lado* L){
    No_lado* novo=NULL;
    novo=(No_lado*)malloc(sizeof(No_lado));

    novo->prox_cima=NULL;
    novo->prox_lista=NULL;
    novo=L;
    return novo;
}

No_lado* insere_fim_lado(No_lado* Lista_m, No_lado* L){
    No_lado* novo=cria_no_lado(L);
    No_lado* aux=Lista_m;

    if (Lista_m == NULL){
        Lista_m=novo;
    }else{
        while (aux->prox_lista != NULL){
            aux = aux->prox_lista;
        }
        aux->prox_lista=novo;
    }
    return Lista_m;
}

Contagem* contagem_de_genes(No_lado* L){
    int *A = (int*)malloc(sizeof(int));
    int *T = (int*)malloc(sizeof(int));
    int *C = (int*)malloc(sizeof(int));
    int *G = (int*)malloc(sizeof(int));
    *A=*T=*C=*G=0;
    char *letra;
    char palavra[10];
    No_lado* aux = L;
    Contagem *L_gene = NULL;
    Contagem vet[4];


    while (aux != NULL){
        strcpy(palavra,aux->dna);
        letra = strchr(palavra,palavra[0]);

        if (*letra == 'A'){
            (*A)++;
        }

        if (*letra == 'T'){
            (*T)++;
        }

        if (*letra == 'C'){
            (*C)++;
        }

        if (*letra == 'G'){
            (*G)++;
        }

        //============================

        if(*(letra+2) == 'A'){
            (*A)++;
        }

        if(*(letra+2) == 'T'){
            (*T)++;
        }

        if(*(letra+2) == 'C'){
            (*C)++;
        }

        if(*(letra+2) == 'G'){
            (*G)++;
        }
        aux=aux->prox_cima;
    }
        
    strcpy(vet[0].gene,"A");
    vet[0].n=*A;

    strcpy(vet[1].gene,"T");
    vet[1].n=*T;

    strcpy(vet[2].gene,"C");
    vet[2].n=*C;
    
    strcpy(vet[3].gene,"G");
    vet[3].n=*G;

    Contagem maior;
    int j;
    for (int i = 0; i < 4; i++){
        j=i;
        while (vet[j].n>vet[j+1].n){
            maior=vet[j+1];
            vet[j+1]=vet[j];
            vet[j]=maior;
            j--;
        }
    }
    for (int i = 3; i != -1; i--){
        L_gene = insere_comeco_gene(L_gene,vet[i].n,vet[i].gene);
    }
    
    free(A);
    free(T);
    free(C);
    free(G);
    return L_gene;
}

Contagem* cria_no_gene(int valor, char gene[]){
    Contagem* novo=NULL;
    novo=(Contagem*)malloc(sizeof(Contagem));

    novo->lado=NULL;
    novo->prox=NULL;
    novo->n=valor;
    strcpy(novo->gene,gene);
    return novo;
}

Contagem* insere_comeco_gene(Contagem* L_gene, int valor, char gene[]){
    Contagem* novo=cria_no_gene(valor,gene);
    novo->prox=L_gene;
    L_gene=novo;
    return L_gene;
}

Contagem* cria_no_gene_lado(Contagem* count){
    Contagem* novo=NULL;
    novo=(Contagem*)malloc(sizeof(Contagem));

    novo->lado=NULL;
    novo->prox=NULL;
    novo=count;
    return novo;
}

Contagem* insere_fim_gene(Contagem* count_m, Contagem* count){
    Contagem* novo=cria_no_gene_lado(count);
    Contagem* aux=count_m;

    if (count_m == NULL){
        count_m=novo;
    }else{
        while (aux->lado != NULL){
            aux = aux->lado;
        }
        aux->lado=novo;
    }
    return count_m;
}

void mostra_genes(Contagem* count_m){
    Contagem *aux_lado = count_m;
    Contagem *aux_cima = count_m;
    while (aux_lado->lado != NULL){
        while (aux_cima != NULL){
            printf("%s %i\n",(aux_cima->gene),(aux_cima->n));
            aux_cima = aux_cima->prox;
        }
        printf("\n");
        aux_lado = aux_lado->lado;
        aux_cima = aux_lado;
    }
}